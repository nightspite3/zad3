---
author: Kacper Kurteczka
title: Mr Robot
subtitle: Podstawowe informacje
date: 7 listopada 2020
theme: Warsaw
output: beamer_presentation
---

## Mr Robot 

![mr_robot](assets/mrRobot.png)

## Wstęp

**Mr Robot** - amerykański serial telewizyjny wyprodukowany przez Universal Cable Productions oraz Anonymous Content. Twórcą serialu jest **Sam Esmail**, a producentem wykonawczym **Chad Hamilton**. Serial był emitowany od 24 czerwca 2015 przez USA Network i doczekał się 45 odcinków.

## Fabuła

**Elliot (Rami Malek)** to cierpiący na fobię społeczną, niewyróżniający się z tłumu, skryty w sobie programista. Jest specjalistą od cyberbezpieczeństwa w dużej korporacji AllSafe. Pewnego dnia spotyka tajemniczego anarchistę i hakera, tytułowego Mr. Robota. Nie wiadomo, kim jest ani skąd się wziął, ale przyświeca mu jeden cel - chce zniszczyć korporacyjną Amerykę. I z pomocą Elliota jest w stanie tego dokonać. Od tej chwili rozpoczyna się niebezpieczna gra, w której stawką będzie nie tylko życie bohaterów, ale również przyszłość świata, który znamy. Celem pierwszego ataku hakerów staje się bowiem potężna korporacja E Corp, która nie cofnie się przed niczym, aby bronić swoich interesów.

## Fabuła

Głównym bohaterem serialu jest **Elliot Alderson** – młody, aspołeczny haker nastawiony negatywnie do wielkich korporacji. Jego problemy emocjonalne znacznie utrudniają kontakty międzyludzkie. Najłatwiejszą drogę do "kontaktu" z innymi bohater odnajduje w hakowaniu ich komputerów. Przez swoje umiejętności, stanowisko specjalisty ds. bezpieczeństwa w firmie AllSafe oraz nastawienie do świata wzbudza zainteresowanie **fsociety** – podziemnej grupy anarchistycznych crackerów. Przez taki obrót spraw bohater za dnia jest niewyróżniającym się pracownikiem korporacji, a w nocy hakerem walczącym o wolność społeczeństwa.

## Obsada

### Główni bohaterowie:

- Rami Malek jako Elliot Alderson, programista, haker
- Christian Slater jako Mr. Robot
- Portia Doubleday jako Angela Moss
- Carly Chaikin jako Darlene, hakerka tworząca komputerowe robaki
- Martin Wallström jako Tyrell Wellick, biznesmen
- Grace Gummer jako Dominique „Dom” DiPierro, młoda agentka FBI (sezony 2-4)
- Michael Cristofer jako Phillip Price, szef E Corp (sezony 2-4, drugoplanowo w - sezonie 1)
- Stephanie Corneliussen jako Joanna Wellick (sezony 2-3, drugoplanowo w sezonie 1)
- BD Wong jako Whiterose (sezony 3-4, drugoplanowo w sezonach 1-2)
- Bobby Cannavale jako Irving (sezon 3)
- Elliot Villar jako Fernando Vera (sezon 4)

## Lista odcinków

| Sezon | Odcinki |   Premiera sezonu    |   Finał sezonu   |
| :---: | :-----: | :------------------: | :--------------: |
|   1   |   10    |   24 czerwca 2015    | 2 września 2015  |
|   2   |   12    |    13 lipca 2016     | 21 września 2016 |
|   3   |   10    | 11 października 2017 | 13 grudnia 2017  |
|   4   |   13    | 6 października 2019  | 22 grudnia 2019  |

## Nagrody
Serial był nominowany do wielu nagród w różnych kategoriach, między innymi:

### Złote Globy:

1. Złoty Glob 2016 - wygrana : Najlepszy serial dramatyczny
2. Złoty Glob 2016 - wygrana : Najlepszy aktor drugoplanowy w serialu, serialu limitowanym lub filmie telewizyjnym Christian Slater
3. Złoty Glob 2016 - nominacja : Najlepszy aktor w serialu dramatycznym Rami Malek

## Nagrody

### Nagrody Emmy:

1. Emmy 2016 - wygrana : Najlepszy aktor w serialu dramatycznym Rami Malek
2. Emmy 2016 - wygrana : Najlepsza kompozycja muzyczna w serialu (oryginalna dramatyczna ścieżka dźwiękowa) Mac Quayle za odcinek pilotażowy
3. Emmy 2016 - nominacja : Najlepszy serial dramatyczny
4. Emmy 2016 - nominacja : Najlepszy dobór obsady serialu dramatycznego
5. Emmy 2016 - nominacja : Najlepszy dźwięk w serialu komediowym lub dramatycznym (godzinnym) Andrew Morgado, John W. Cook II, Timothia Sellers, William Freesh za odcinek "eps1.5_br4ve-trave1er.asf"
6. Emmy 2016 - nominacja : Najlepszy scenariusz serialu dramatycznego Sam Esmail za odcinek pilotażowy

## Nagrody

### People's Choice:

1. People's Choice 2016 - nominacja : Ulubiony aktor w telewizji kablowej Christian Slater

### Satelity:

1. Satelity 2016 - wygrana : Najlepszy aktor drugoplanowy w serialu, miniserialu lub filmie telewizyjnym Christian Slater
2. Satelity 2016 - nominacja : Najlepszy serial dramatyczny
3. Satelity 2016 - nominacja : Najlepszy aktor w serialu dramatycznym Rami Malek

### Złote szpule:

1. Złota szpula 2016 - wygrana : Najlepszy montaż muzyki w odcinku serialu za odcinek "hellofriend.mov"